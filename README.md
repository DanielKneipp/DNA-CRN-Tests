# Statistical Analysis of DNA Reactions

This repository has statistical analysis about the behavior of a formal chemical reaction network (CRN) in comparison of the same CRNs implemented using DNA following the approach described by **[1]**.

There is a report in the `report` folder (in Brazilian Portuguese) describing all the analysis made and obtained results.

## References

- **[1]** Soloveichik, David, Georg Seelig, and Erik Winfree. "DNA
as a universal substrate for chemical kinetics." Proceedings of the
National Academy of Sciences 107.12 (2010): 5393-5398.
