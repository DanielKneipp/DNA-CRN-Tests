@echo off

:: Generating configurations
echo. & echo Generating configurations & echo.
Rscript gen_config_regress.R
:: Calculating DNAas reactions
echo. & echo Calculating DNAas reactions & echo.
cd DNAas\
Rscript A+B--C_regress.R
:: Calculating formal reactions
echo. & echo Calculating formal reactions & echo.
cd ..\Formal
Rscript A+B--C_regress.R
:: Calculating the erros
echo. & echo Calculating the erros & echo.
cd "..\Diff outs\"
Rscript diff_outs_regress.R

:: Pause
pause