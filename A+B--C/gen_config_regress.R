Cis <- 1e3
Cmaxs <- 1e5
qis <- seq(1e-7 * 0.7, 1e-7 * 1.3, length.out = 50)
qmaxs <- 1e-3
combs <- expand.grid(Cis, Cis, Cmaxs, qis, qmaxs)
colnames(combs) <- c("A", "B", "Cmax", "qi", "qmax")
write.csv(combs, file = 'config_regress.csv', row.names = FALSE)

# To read: c <- read.csv('config.csv', header = TRUE)