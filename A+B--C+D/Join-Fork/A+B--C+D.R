#
# A + B --k--> C + D Formal reaction using Join-Fork approach:
#
# A + J_AB      <--k1,k2-->     J_AB_1 + a_tb   (R1)
# B + J_AB_1    <--k3,k4-->     J_AB_2 + b_tr   (R2)
# J_AB_2 + tr_r <--k5,k6-->     J_AB_3 + r_tq   (R3)
# F_C + r_tq    <--k7,k8-->     F_C_1 + tr_r    (R4)
# F_C_1 + c_tr  <--k9,k10-->    F_C_2 + C       (R5)
# F_C_2 + d_tc  <--k11,k12-->   F_C_3 + D       (R6)
# F_C_3 + i_tc   --k13-->       Fork_C_4 + i    (R7)
#
# J1 = k1[A][J_AB]      - k2[J_AB_1][a_tb]           
# J2 = k3[B][J_AB_1]    - k4[J_AB_2][b_tr]
# J3 = k5[J_AB_2][tr_r] - k6[J_AB_3][r_tq]
# J4 = k7[F_C][r_tq]    - k8[F_C_1][r_tq]
# J5 = k9[F_C_1][c_tr]  - k10[F_C_2][C]
# J6 = k11[F_C_2][d_tc] - k12[F_C_3][D]
# J7 = k13[F_C_3][i_tc]
#
# dA/dt         = -J1
# dB/dt         = -J2
# dC/dt         =  J5
# dD/dt         =  J6
# dJ_AB/dt      = -J1
# dJ_AB_1/dt    =  J1 - J2
# dJ_AB_2/dt    =  J2 - J3
# dJ_AB_3/dt    =  J3
# dF_C/dt       = -J4
# dF_C_1/dt     =  J4 - J5
# dF_C_2/dt     =  J5 - J6
# dF_C_3/dt     =  J6 - J7
# dF_C_4/dt     =  J7
# da_tb/dt      = -J1
# db_tr/dt      = -J2
# dtr_r/dt      = -J3 + J4
# dr_tq/dt      =  J3 - J4
# dc_tr/dt      = -J5
# dd_tc/dt      = -J6
# di_tc/dt      = -J7
# di/dt         =  J7


# Cleaning the workspace
rm(list = ls())

library(deSolve)
library(latex2exp)

fx <- function(t, y, pars) {
    with(as.list(pars), {
        dy      <- numeric(18)
        A       <- y[1]
        B       <- y[2]
        C       <- y[3]
        D       <- y[4]
        J_AB    <- y[5]
        J_AB_1  <- y[6]
        J_AB_2  <- y[7] 
        J_AB_3  <- y[8]
        F_C     <- y[9]
        F_C_1   <- y[10]
        F_C_2   <- y[11]
        F_C_3   <- y[12]
        F_C_4   <- y[13]
        a_tb    <- y[14]
        b_tr    <- y[15]
        tr_r    <- y[16]
        r_tq    <- y[17]
        c_tr    <- y[18]
        d_tc    <- y[19]
        i_tc    <- y[20]
        i       <- y[21]
        
        J1 = k1 * A * J_AB      - k2 * J_AB_1 * a_tb           
        J2 = k3 * B * J_AB_1    - k4 * J_AB_2 * b_tr
        J3 = k5 * J_AB_2 * tr_r - k6 * J_AB_3 * r_tq
        J4 = k7 * F_C * r_tq    - k8 * F_C_1 * r_tq
        J5 = k9 * F_C_1 * c_tr  - k10 * F_C_2 * C
        J6 = k11 * F_C_2 * c_tr  - k12 * F_C_3 * D
        J7 = k11 * F_C_3 * i_tc
        
        dy[1]  <- -J1
        dy[2]  <- -J2
        dy[3]  <-  J5
        dy[4]  <-  J6
        dy[5]  <- -J1
        dy[6]  <-  J1 - J2
        dy[7]  <-  J2 - J3
        dy[8]  <-  J3
        dy[9]  <- -J4
        dy[10] <-  J4 - J5
        dy[11] <-  J5 - J6
        dy[12] <-  J6 - J7
        dy[13] <-  J7
        dy[14] <- -J1
        dy[15] <- -J2
        dy[16] <- -J3 + J4
        dy[17] <-  J3 - J4
        dy[18] <- -J5
        dy[19] <- -J6
        dy[20] <- -J7
        dy[21] <-  J7

        list(dy)
    })
}

Ci_A <- 40.0    # nM
Ci_B <- 40.0    # nM
Cmax <- 13 * 40.0   # nM

y0 <- c(
    Ci_A,   # A
    Ci_B,   # B
    0.0,    # C
    0.0,    # D
    Cmax,   # J_AB
    0.0,    # J_AB_1
    0.0,    # J_AB_2
    0.0,    # J_AB_3
    Cmax,   # F_C
    0.0,    # F_C_1
    0.0,    # F_C_2
    0.0,    # F_C_3
    0.0,    # F_C_4
    Cmax,   # a_tb
    Cmax,   # b_tr
    Cmax,   # tr_r
    0.0,    # r_tq
    Cmax,   # c_tr
    Cmax,   # d_tc
    Cmax,   # i_tc
    0.0     # i
)
t <- seq(0, 72000, 1)   # Seconds
params <- list(
    k1  = 39277e-09,    # M^-1s^-1
    k2  = 510720e-09,  
    k3  = 121200e-09,
    k4  = 158000e-09,
    k5  = 279030e-09,
    k6  = 10197e-09,
    k7  = 208830e-09,
    k8  = 279030e-09,
    k9  = 24785e-09,
    k10 = 33016e-09,
    k11 = 24785e-09,
    k12 = 33016e-09,
    k13 = 47793e-09
)
# k <- k3*((JoinAB*k1)/(JoinAB*k1+a_tb*k2))*((tr_r*k5)/(b_tr*k4+tr_r*k5))

out <- ode(times = t, y = y0, func = fx, parms = params)
A <- out[,2]
B <- out[,3]
C <- out[,4]
D <- out[,5]

#pdf("plot A+B--C.pdf")
par(oma=c(0,0,0,0))
par(mar=c(4.8,5,1,1))
plot(t, A, type = "l", col = "blue", lwd = 6.5, xlab = "Tempo (s)", ylab = "Concentração (nM)", 
     cex = 2, cex.main = 2, cex.lab = 2, cex.axis = 2, cex.main = 2, cex.sub = 2, 
     bty = "n", xlim = c(0, tail(t,n=1)), ylim = c(0, max(c(Ci_A, Ci_B))))
lines(t, B, col = "green", lwd = 6.5, lty = 2)
lines(t, C, col = "red", lwd = 6.5)
lines(t, D, col = "orange", lwd = 6.5, lty = 2)
legend("topright", bg = "white", 
       legend = c(TeX("A"), TeX("B"), TeX("C"), TeX("D")), 
       col = c("blue", "green", "red", "orange"), lty=c(1,2,1,2), lwd=c(6.5,6.5,6.5,6.5), cex = 1.5)
#dev.off()
