#
# A + B --k--> C + D
#
# J = k[A][B]
#
# dA/dt = -J
# dB/dt = -J
# dC/dt = J
# dD/dt = J

# Cleaning the workspace
rm(list = ls())

library(deSolve)
library(latex2exp)

fx <- function(t, y, pars) {
    with(as.list(pars), {
        dy <- numeric(3)
        A <- y[1]
        B <- y[2]
        C <- y[3]
        D <- y[4]
        
        J <- k * A * B
        
        dy[1] <- -J
        dy[2] <- -J
        dy[3] <- J
        dy[4] <- J
        
        list(dy)
    })
}

y0 <- c(1e4, 1e3, 0.0, 0.0)
t <- seq(0, 72000, 1)
params <- list(
    k <- 1e-7
)

plot_result <- function(t, A, B, C, D, Ci_A, Ci_B) {
    #pdf("plot A+B--C+D.pdf")
    par(oma=c(0,0,0,0))
    par(mar=c(4.8,5,1,1))
    plot(t, A, type = "l", col = "blue", lwd = 6.5, xlab = "Tempo (s)", ylab = "Concentração (nM)", 
         cex = 2, cex.main = 2, cex.lab = 2, cex.axis = 2, cex.main = 2, cex.sub = 2, 
         bty = "n", xlim = c(0, tail(t,n=1)), ylim = c(0, max(y0)))
    lines(t, B, col = "green", lwd = 6.5, lty = 2)
    lines(t, C, col = "red", lwd = 6.5)
    lines(t, D, col = "orange", lwd = 6.5, lty = 2)
    legend("topright", bg = "white", 
        legend = c(TeX("A"), TeX("B"), TeX("C"), TeX("D")), 
        col = c("blue", "green", "red", "orange"), lty=c(1,2,1,2), lwd=c(6.5,6.5,6.5,6.5), cex = 1.5)
    #dev.off()
}

# out <- ode(times = t, y = y0, func = fx, parms = params)
# A <- out[,2]
# B <- out[,3]
# C <- out[,4]
# D <- out[,5]
# plot_result(t, A, B, C, D, Ci_A, Ci_B)

run_save <- function(times, y0, func, parms, cols_to_save, col_names, file_name) {
    out <- ode(times = times, y = y0, func = func, parms = parms)
    matrix_to_save = out[,cols_to_save]
    colnames(matrix_to_save) <- col_names
    write.csv(matrix_to_save, file = file_name, row.names = FALSE)
}

# y0 <- c(40.0, 40.0, 0.0, 0.0)
# run_save(t, y0, fx, params, c(2,3,4,5), c("A", "B", "C", "D"), "A_40+B_40--C+D.csv")

# y0 <- c(0.0, 40.0, 0.0, 0.0)
# run_save(t, y0, fx, params, c(2,3,4,5), c("A", "B", "C", "D"), "A_0+B_40--C+D.csv")

# y0 <- c(40.0, 0.0, 0.0, 0.0)
# run_save(t, y0, fx, params, c(2,3,4,5), c("A", "B", "C", "D"), "A_40+B_0--C+D.csv")

# y0 <- c(0.0, 0.0, 0.0, 0.0)
# run_save(t, y0, fx, params, c(2,3,4,5), c("A", "B", "C", "D"), "A_0+B_0--C+D.csv")

#Ci <- seq(0, 40, 5)
#Cis <- expand.grid(Ci, Ci)
combs <- read.csv('../config.csv', header = TRUE)
Cis <- unique(combs[, c(1, 2)])
for(i in 1:dim(Cis)[1]) {
    y0 <- c(Cis[i,1], Cis[i,2], 0.0, 0.0)
    str <- sprintf("A_%1.2e+B_%1.2e--C+D.csv", Cis[i,1], Cis[i,2])
    cat(paste("Running for", str, "\n", sep = ""))
    run_save(t, y0, fx, params, c(2,3,4,5), c("A", "B", "C", "D"), str)
}
