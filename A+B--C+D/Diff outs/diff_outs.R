rmse <- function(sim, obs) {
    return(sqrt(mean((obs - sim)^2)))
}

nrmse_1 <- function(rmse, obs) {
    return(rmse / (1 + max(obs) - min(obs)))
}

diff_outs <- function(file_sim, file_obs) {
    data_sim <- read.csv(file_sim, header = TRUE)
    data_obs <- read.csv(file_obs, header = TRUE)
    
    A_error <- nrmse_1(rmse(data_sim[,'A'], data_obs[,'A']), data_obs[,'A'])
    B_error <- nrmse_1(rmse(data_sim[,'B'], data_obs[,'B']), data_obs[,'B'])
    C_error <- nrmse_1(rmse(data_sim[,'C'], data_obs[,'C']), data_obs[,'C'])
    D_error <- nrmse_1(rmse(data_sim[,'D'], data_obs[,'D']), data_obs[,'D'])
    
    total_error <- (A_error + B_error + C_error + D_error) / 4
    return(total_error)
}

# To DNAas (same for Join-Fork in another table):
#  A   B    error
# 40  40     35
#  0  40     30
#  0   0     12

# Ci <- seq(0, 40, 5)
# Cis <- expand.grid(Ci, Ci)
combs <- read.csv('../config.csv', header = TRUE)

dnaas_result <- matrix(nrow = dim(combs)[1], ncol = 6)
colnames(dnaas_result) <- c('A', 'B', 'Cmax', 'qi', 'qmax', 'error')

cat("Calculating erros\n")
for(i in 1:dim(combs)[1]) {
    dnaas_result[i,1] <- combs[i,1]
    dnaas_result[i,2] <- combs[i,2]
    dnaas_result[i,3] <- combs[i,3]
    dnaas_result[i,4] <- combs[i,4]
    dnaas_result[i,5] <- combs[i,5]
    str_formal <- sprintf('A_%1.2e+B_%1.2e--C+D.csv', combs[i,1], combs[i,2])
    str_dnaas <- sprintf("A_%1.2e+B_%1.2e--C+D Cmax_%1.2e qi_%1.2e qmax_%1.2e.csv", combs[i,1], combs[i,2], combs[i,3], combs[i,4], combs[i,5])
    dnaas_result[i,6] <- diff_outs(paste('../Formal/', str_formal, sep = ''), paste('../DNAas/', str_dnaas, sep = ''))
}
cat("Erros calculated\n")

# dnaas_result[1,1] <- 40.0
# dnaas_result[1,2] <- 40.0
# dnaas_result[1,3] <- diff_outs('../Formal/A_40+B_40--C+D.csv', '../DNAas/A_40+B_40--C+D.csv')

# dnaas_result[2,1] <- 0.0
# dnaas_result[2,2] <- 40.0
# dnaas_result[2,3] <- diff_outs('../Formal/A_0+B_40--C+D.csv', '../DNAas/A_0+B_40--C+D.csv')

# dnaas_result[3,1] <- 40.0
# dnaas_result[3,2] <- 0.0
# dnaas_result[3,3] <- diff_outs('../Formal/A_40+B_0--C+D.csv', '../DNAas/A_40+B_0--C+D.csv')

# dnaas_result[4,1] <- 0.0
# dnaas_result[4,2] <- 0.0
# dnaas_result[4,3] <- diff_outs('../Formal/A_0+B_0--C+D.csv', '../DNAas/A_0+B_0--C+D.csv')

write.csv(dnaas_result, file = 'DNAas_A+B--C+D_erros.csv', row.names = FALSE)

# TODO: Same for Join-Fork
