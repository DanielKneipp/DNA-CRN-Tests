@echo off

:: Calculate the erros for each reaction
echo. & echo Calculating erros for reaction A + B --^> C & echo.
call :RUN_DIFFS "A+B--C"
echo. & echo Calculating erros for reaction A + B --^> C + D & echo.
call :RUN_DIFFS "A+B--C+D"
echo. & echo Calculating erros for reaction A --^> B & echo.
call :RUN_DIFFS "A--B"
echo. & echo Calculating erros for reaction A --^> B + C & echo.
call :RUN_DIFFS "A--B+C"

:: Get all results
echo. & echo Putting all errors together & echo.
cd "All diffs\"
Rscript "get_all_diffs.R"

echo. & echo Calculating 2k & echo.
Rscript "2k.R"

:: Pause to see output
pause

:: Exit
echo. & echo Done & echo.
goto :eof


:RUN_DIFFS
    set react_name=%~1

    :: Get the current path of this script
    set curr_path=%~dp0

    cd "%react_name%\DNAas\"
    Rscript "%react_name%.R"
    cd "..\Formal\"
    Rscript "%react_name%.R"
    cd "..\Diff outs\"
    Rscript "diff_outs.R"

    cd %curr_path%

    exit /b 0