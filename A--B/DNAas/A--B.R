#
# A --k--> B Formal reaction using 2-domain approach:
#
# A + Gi --qi--> Oi     (R1)
# Oi + Ti --qmax--> B   (R2)
#
# J1 = qi[A][Gi]
# J2 = qmax[Oi][Ti]
#
# dA/dt  = -J1
# dB/dt  =  J2
# dGi/dt = -J1
# dOi/dt =  J1 - J2
# dTi/dt = -J2

# Cleaning the workspace
rm(list = ls())

library(deSolve)
library(latex2exp)

fx <- function(t, y, pars) {
    with(as.list(pars), {
        dy <- numeric(5)
        A  <- y[1]
        B  <- y[2]
        Gi <- y[3]
        Oi <- y[4]
        Ti <- y[5]
        
        J1 <- qi * A * Gi
        J2 <- qmax * Oi * Ti

        dy[1] <- -J1
        dy[2] <-  J2
        dy[3] <- -J1
        dy[4] <-  J1 - J2
        dy[5] <- -J2
        
        list(dy)
    })
}

Ci_A <- 1e4    # nM
Cmax <- 1e5   # nM

#         A    B    Gi    Oi   Ti
y0 <- c(Ci_A, 0.0, Cmax, 0.0, Cmax)
t <- seq(0, 72000, 1)   # Seconds
# ki = qi * Cmax
# qi = ki / Cmax
params <- list(
    qi   = 5e-5 / Cmax,    # M^-1s^-1
    qmax = 1e-5               # M^-1s^-1
)

plot_result <- function(t, A, B, Ci_A) {
    #pdf("plot A--B.pdf")
    par(oma=c(0,0,0,0))
    par(mar=c(4.8,5,1,1))
    plot(t, A, type = "l", col = "blue", lwd = 6.5, xlab = "Tempo (s)", ylab = "Concentração (nM)", 
         cex = 2, cex.main = 2, cex.lab = 2, cex.axis = 2, cex.main = 2, cex.sub = 2, 
         bty = "n", xlim = c(0, tail(t,n=1)), ylim = c(0, Ci_A))
    lines(t, B, col = "red", lwd = 6.5, lty = 1)
    legend("topright", bg = "white", 
           legend = c(TeX("A"), TeX("B")), 
           col = c("blue", "red"), lty=c(1,1), lwd=c(6.5,6.5), cex = 1.5)
    #dev.off()
}

# out <- ode(times = t, y = y0, func = fx, parms = params)
# A <- out[,2]
# B <- out[,3]
# plot_result(t, A, B, Ci_A)

run_save <- function(times, y0, func, parms, cols_to_save, col_names, file_name) {
    out <- ode(times = times, y = y0, func = func, parms = parms)
    matrix_to_save = out[,cols_to_save]
    colnames(matrix_to_save) <- col_names
    write.csv(matrix_to_save, file = file_name, row.names = FALSE)
}

# y0 <- c(40.0, 0.0, Cmax, 0.0, Cmax)
# run_save(t, y0, fx, params, c(2,3), c("A", "B"), "A_40--B.csv")

# y0 <- c(0.0, 0.0, Cmax, 0.0, Cmax)
# run_save(t, y0, fx, params, c(2,3), c("A", "B"), "A_0--B.csv")

# Cis <- data.matrix(read.csv('../config.csv', header = T)[1])
combs <- read.csv('../config.csv', header = TRUE)
for(i in 1:dim(combs)[1]) {
    y0 <- c(combs[i,1], 0.0, combs[i,3], 0.0, combs[i,3])
    params <- list(
        qi   = combs[i,4],    # M^-1s^-1
        qmax = combs[i,5]    # M^-1s^-1
    )
    str <- sprintf('A_%1.2e--B Cmax_%1.2e qi_%1.2e qmax_%1.2e.csv', combs[i,1], combs[i,3], combs[i,4], combs[i,5])
    cat(paste("Running for ", str, "\n", sep = ""))
    run_save(t, y0, fx, params, c(2,3), c("A", "B"), str)
}
