Cis <- c(0, 1e3, 1e4)
Cmaxs <- seq(1e5 * 0.9, 1e5 * 1.1, length.out = 2)
qis <- seq(5e-10 * 0.9, 5e-10 * 1.1, length.out = 2)
qmaxs <- seq(1e-3 * 0.9, 1e-3 * 1.1, length.out = 2)
combs <- expand.grid(Cis, NA, Cmaxs, qis, qmaxs)
colnames(combs) <- c("A", "B", "Cmax", "qi", "qmax")
write.csv(combs, file = 'config.csv', row.names = FALSE)

# To read: c <- read.csv('config.csv', header = TRUE)