#
# Simple Example: A -> B
#

# Cleaning the workspace
rm(list = ls())

library(deSolve)

fx <- function(t, x, pars) {
    with(as.list(pars), {
        dy <- numeric(2)
        
        dy[1] <- -k*x[1] # x1 = [A]
        dy[2] <- k*x[1]
        
        list(dy) # Output
    })
}

y0 <- c(1e4, 0.0)          # Initial concentrations
t <- seq(0, 72000, 1)      # Time range
params <- list(k = 5e-5)   # Parameters of the system

plot_result <- function(t, A, B, Ci_A) {
    #pdf("plot A--B.pdf")
    par(oma=c(0,0,0,0))
    par(mar=c(4.8,5,1,1))
    plot(t, A, type = "l", col = "blue", lwd = 6.5, xlab = "Tempo", ylab = "Concentração", 
         cex = 2, cex.main = 2, cex.lab = 2, cex.axis = 2, cex.main = 2, cex.sub = 2,
         bty = "n", xlim = c(0, tail(t,n=1)), ylim = c(0, max(y0))) #, xaxt = "n", yaxt = "n", axes=FALSE)
    lines(t, B, col = "red", lwd = 6.5, lty = 1)
    legend("topright", bg = "white", 
           legend = c("A", "B"), 
           col = c("blue", "red"), lty=c(1,1), lwd = c(6.5,6.5), cex = 1.5)
    #dev.off()
}

# out <- ode(times = t, y = y0, func = fx, parms = params)
# A <- out[,2]
# B <- out[,3]
# plot_result(t, A, B, Ci_A)

run_save <- function(times, y0, func, parms, cols_to_save, col_names, file_name) {
    out <- ode(times = times, y = y0, func = func, parms = parms)
    matrix_to_save = out[,cols_to_save]
    colnames(matrix_to_save) <- col_names
    write.csv(matrix_to_save, file = file_name, row.names = FALSE)
}

# y0 <- c(40.0, 0.0)
# run_save(t, y0, fx, params, c(2,3), c("A", "B"), "A_40--B.csv")

# y0 <- c(0.0, 0.0)
# run_save(t, y0, fx, params, c(2,3), c("A", "B"), "A_0--B.csv")

# Cis <- data.matrix(read.csv('../config.csv', header = T)[1])
combs <- read.csv('../config.csv', header = TRUE)
Cis <- unique(combs[, 1])
for(i in 1:length(Cis)) {
    y0 <- c(Cis[i], 0.0)
    str <- sprintf('A_%1.2e--B.csv', Cis[i])
    cat(paste("Running for", str, "\n", sep = ""))
    run_save(t, y0, fx, params, c(2,3), c("A", "B"), str)
}
